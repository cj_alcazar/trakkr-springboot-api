package trakkr.samples

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
class HelloWorldHandler {
    @GetMapping("/hello")
    fun hello(): String = "Hello World!"

    @GetMapping("api/now")
    fun now():String = LocalDate.now().toString()
}