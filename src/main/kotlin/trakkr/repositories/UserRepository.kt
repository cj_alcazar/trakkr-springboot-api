package trakkr.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import trakkr.entities.User

// Extends CrudRepository Interface to easily access the basic DB operations

@Repository
interface UserRepository: CrudRepository<User, Long>